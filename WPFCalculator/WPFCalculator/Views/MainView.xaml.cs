﻿using System.Windows;

namespace WPFCalculator.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            switch (WindowState)
            {
                case WindowState.Minimized:
                    Left = 0;
                    Top = 0;
                    WindowState = WindowState.Maximized;
                    break;
                case WindowState.Normal:
                case WindowState.Maximized:
                    WindowState = WindowState.Minimized;
                    break;
            }
        }
    }
}
