﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace WPFCalculator.Services
{
    /// <summary>
    /// Provides changing a font size if the text length is too long
    /// </summary>
    class AdaptiveFontSizeConverter : IMultiValueConverter
    {
        /// <summary>
        /// Changes the size of the result text
        /// </summary>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var textBlock = (TextBlock)values[0];
            var dpiX = 296.0 * VisualTreeHelper.GetDpi(textBlock).DpiScaleX;
            var formattedText =
                new FormattedText(
                    textBlock.Text,
                    CultureInfo.InvariantCulture,
                    textBlock.FlowDirection,
                    new Typeface(
                        textBlock.FontFamily,
                        textBlock.FontStyle,
                        textBlock.FontWeight,
                        textBlock.FontStretch),
                    textBlock.FontSize,
                    textBlock.Foreground,
                    dpiX);

            var fontSize = textBlock.FontSize * 250 / formattedText.Width;
            if (parameter == null)
            {
                return fontSize;
            }

            var maxSize = (double)parameter;
            return Math.Min(fontSize, maxSize);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
            => throw new NotSupportedException();
    }
}