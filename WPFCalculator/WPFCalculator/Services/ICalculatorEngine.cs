﻿namespace WPFCalculator.Services
{
    /// <summary>
    /// Represents the functionality of the calculator
    /// </summary>
    public interface ICalculatorEngine
    {
        /// <summary>
        /// Returns sum of two operands
        /// </summary>
        double Sum(double firstOperand, double secondOperand);

        /// <summary>
        /// Returns subtraction of two operands
        /// </summary>
        double Subtraction(double firstOperand, double secondOperand);

        /// <summary>
        /// Returns multiplication of two operands
        /// </summary>
        double Multiplication(double firstOperand, double secondOperand);

        /// <summary>
        /// Returns division of two operands
        /// </summary>
        double Division(double firstOperand, double secondOperand);

        /// <summary>
        /// Returns percent of the operand
        /// </summary>
        double Percent(double operand);

        /// <summary>
        /// Changes the sign of operand
        /// </summary>
        double SignChange(double operand);
    }
}