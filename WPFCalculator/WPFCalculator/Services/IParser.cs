﻿namespace WPFCalculator.Services
{
    /// <summary>
    /// The converter
    /// </summary>
    public interface IParser
    {
        /// <summary>
        /// Converts text to double
        /// </summary>
        double ToDouble(string text);

        /// <summary>
        /// Converts double to text 
        /// </summary>
        string ToString(double number);
    }
}