﻿using log4net;
using log4net.Config;

namespace WPFCalculator.Services
{
    /// <summary>
    /// The logger
    /// </summary>
    public static class Logger
    {
        /// <summary>
        /// Contains methods for logging
        /// </summary>
        public static ILog Log { get; } = LogManager.GetLogger("LOGGER");

        /// <summary>
        /// Initializes a logger
        /// </summary>
        public static void InitLogger()
        {
            XmlConfigurator.Configure();
        }
    }
}