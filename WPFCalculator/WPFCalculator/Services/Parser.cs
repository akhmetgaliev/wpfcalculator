﻿using System.Globalization;

namespace WPFCalculator.Services
{
    /// <summary>
    /// The converter
    /// </summary>
    public class Parser : IParser
    {
        /// <summary>
        /// Converts text to double
        /// </summary>
        public double ToDouble(string text)
        {
            return double.Parse(text);
        }

        /// <summary>
        /// Converts double to text 
        /// </summary>
        public string ToString(double number)
        {
            return number.ToString(CultureInfo.InvariantCulture);
        }
    }
}