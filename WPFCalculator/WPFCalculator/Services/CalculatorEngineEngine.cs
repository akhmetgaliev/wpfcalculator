﻿namespace WPFCalculator.Services
{
    /// <summary>
    /// Represents the functionality of the calculator
    /// </summary>
    public class CalculatorEngineEngine : ICalculatorEngine
    {
        /// <summary>
        /// Returns sum of two operands
        /// </summary>
        public double Sum(double firstOperand, double secondOperand)
        {
            return firstOperand + secondOperand;
        }

        /// <summary>
        /// Returns subtraction of two operands
        /// </summary>
        public double Subtraction(double firstOperand, double secondOperand)
        {
            return firstOperand - secondOperand;
        }

        /// <summary>
        /// Returns multiplication of two operands
        /// </summary>
        public double Multiplication(double firstOperand, double secondOperand)
        {
            return firstOperand * secondOperand;
        }

        /// <summary>
        /// Returns division of two operands
        /// </summary>
        public double Division(double firstOperand, double secondOperand)
        {
            return firstOperand / secondOperand;
        }

        /// <summary>
        /// Returns percent of the operand
        /// </summary>
        public double Percent(double operand)
        {
            return operand / 100;
        }

        /// <summary>
        /// Changes the sign of operand
        /// </summary>
        public double SignChange(double operand)
        {
            return -operand;
        }
    }
}