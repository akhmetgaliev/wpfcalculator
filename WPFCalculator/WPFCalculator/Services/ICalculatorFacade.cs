﻿using WPFCalculator.Common;

namespace WPFCalculator.Services
{
    /// <summary>
    /// Encapsulates the logic of calculator
    /// </summary>
    public interface ICalculatorFacade
    {
        /// <summary>
        /// Processes the operands by the necessary action
        /// </summary>
        /// <param name="firstOperand">The first operand</param>
        /// <param name="secondOperand">The second operand</param>
        /// <param name="actionType">The type of the necessary action</param>
        /// <returns>Result of the action</returns>
        double Process(double firstOperand, double secondOperand, ActionType actionType);
    }
}