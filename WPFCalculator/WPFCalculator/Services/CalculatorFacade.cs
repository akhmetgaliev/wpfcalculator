﻿using WPFCalculator.Common;
using WPFCalculator.Properties;

namespace WPFCalculator.Services
{
    /// <summary>
    /// Encapsulates the logic of calculator
    /// </summary>
    public class CalculatorFacade : ICalculatorFacade
    {
        /// <summary>
        /// Represents the functionality of the calculator
        /// </summary>
        private readonly ICalculatorEngine _calculator;

        /// <summary>
        /// Creates instance of <see cref="CalculatorFacade"/>
        /// </summary>
        /// <param name="calculator">Represents the functionality of the calculator</param>
        public CalculatorFacade(ICalculatorEngine calculator)
        {
            _calculator = calculator;
        }

        /// <summary>
        /// Processes the operands by the necessary action
        /// </summary>
        /// <param name="firstOperand">The first operand</param>
        /// <param name="secondOperand">The second operand</param>
        /// <param name="actionType">The type of the necessary action</param>
        /// <returns>Result of the action</returns>
        public double Process(double firstOperand, double secondOperand, ActionType actionType)
        {
            double result = 0;
            switch (actionType)
            {
                case ActionType.Sum:
                    result = _calculator.Sum(firstOperand, secondOperand);
                    break;
                case ActionType.Subtraction:
                    result = _calculator.Subtraction(firstOperand, secondOperand);
                    break;
                case ActionType.Division:
                    result = _calculator.Division(firstOperand, secondOperand);
                    break;
                case ActionType.Multiplication:
                    result = _calculator.Multiplication(firstOperand, secondOperand);
                    break;
            }

            var message = string.Format(Resources.ActionMessage, actionType, firstOperand, secondOperand, result);
            Logger.Log.Info(message);

            return result;
        }
    }
}