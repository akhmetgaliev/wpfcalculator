﻿using Autofac;
using Caliburn.Micro;

namespace WPFCalculator.Services
{
    /// <summary>
    /// Provides the instance of the specified viewModel
    /// </summary>
    public class ViewModelProvider : IViewModelProvider
    {
        /// <summary>
        /// Container of the registered types
        /// </summary>
        private readonly ILifetimeScope _container;

        /// <summary>
        /// Creates instance of <see cref="ViewModelProvider"/>
        /// </summary>
        /// <param name="container"></param>
        public ViewModelProvider(ILifetimeScope container)
        {
            _container = container;
        }

        /// <summary>
        /// Gets the necessary viewModel
        /// </summary>
        /// <typeparam name="T">Type of the necessary view model</typeparam>
        /// <returns>The necessary viewModel</returns>
        public T GetViewModel<T>() where T : PropertyChangedBase
        {
            return _container.Resolve<T>();
        }
    }
}