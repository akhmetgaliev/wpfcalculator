﻿using Caliburn.Micro;

namespace WPFCalculator.Services
{
    /// <summary>
    /// Provides the instance of the specified viewModel
    /// </summary>
    public interface IViewModelProvider
    {
        /// <summary>
        /// Gets the necessary viewModel
        /// </summary>
        /// <typeparam name="T">Type of the necessary view model</typeparam>
        /// <returns>The necessary viewModel</returns>
        T GetViewModel<T>() where T : PropertyChangedBase;
    }
}