﻿using System;
using System.Windows;
using Autofac;
using Caliburn.Micro;
using WPFCalculator.Services;
using WPFCalculator.ViewModels;
using Parser = WPFCalculator.Services.Parser;

namespace WPFCalculator
{
    /// <summary>
    /// Configures an IoC container, starts the main window
    /// </summary>
    public class Bootstrapper : BootstrapperBase
    {
        /// <summary>
        /// The IoC container
        /// </summary>
        private IContainer _container;

        /// <summary>
        /// Initializes a bootstrapper
        /// </summary>
        public Bootstrapper()
        {
            Initialize();
        }

        /// <summary>
        /// Setups an IoC container.
        /// </summary>
        protected override void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<CalculatorEngineEngine>().As<ICalculatorEngine>().SingleInstance();
            builder.RegisterType<CalculatorFacade>().As<ICalculatorFacade>().SingleInstance();
            builder.RegisterType<WindowManager>().As<IWindowManager>().SingleInstance();
            builder.RegisterType<Parser>().As<IParser>().SingleInstance();
            builder.RegisterType<ViewModelProvider>().As<IViewModelProvider>().SingleInstance();
            builder.RegisterType<MessageViewModel>().SingleInstance();
            builder.RegisterType<MainViewModel>().SingleInstance();
            builder.RegisterType<SimpleCalculatorViewModel>().SingleInstance();

            _container = builder.Build();
        }

        /// <summary>
        /// Provides the necessary instance from an IoC container.
        /// </summary>
        /// <param name="service">The service to locate.</param>
        /// <param name="key">The key to locate.</param>
        /// <returns>The located service.</returns>
        protected override object GetInstance(Type service, string key)
        {
            return _container.Resolve(service);
        }

        /// <summary>
        /// Behavior to execute after the application starts.
        /// </summary>
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            Logger.InitLogger();
            DisplayRootViewFor<MainViewModel>();
        }
    }
}