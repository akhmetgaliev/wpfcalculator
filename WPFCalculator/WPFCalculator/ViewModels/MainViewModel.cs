﻿using Caliburn.Micro;
using WPFCalculator.Services;

namespace WPFCalculator.ViewModels
{
    /// <summary>
    /// The frame of calculators
    /// </summary>
    public class MainViewModel : PropertyChangedBase
    {
        /// <summary>
        /// Creates instance of <see cref="MainViewModel"/>
        /// </summary>
        /// <param name="viewModelProvider">Provides the instance of the specified viewModel</param>
        public MainViewModel(IViewModelProvider viewModelProvider)
        {
            CalculatorsFrame = viewModelProvider.GetViewModel<SimpleCalculatorViewModel>();
        }

        /// <summary>
        /// Provides binding with the specified calculator
        /// </summary>
        public PropertyChangedBase CalculatorsFrame { get; set; }
    }
}