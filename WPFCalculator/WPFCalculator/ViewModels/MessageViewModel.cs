﻿using Caliburn.Micro;

namespace WPFCalculator.ViewModels
{
    /// <summary>
    /// Provides viewModel for showing some message
    /// </summary>
    public class MessageViewModel : PropertyChangedBase
    {
        /// <summary>
        /// Text of the message block
        /// </summary>
        private string _messageBlock;

        /// <summary>
        /// Message block text
        /// </summary>
        public string MessageBlock
        {
            get => _messageBlock;
            set
            {
                _messageBlock = value;
                NotifyOfPropertyChange(() => MessageBlock);
            }
        }
    }
}