﻿using System.Threading.Tasks;
using Caliburn.Micro;
using WPFCalculator.Common;
using WPFCalculator.Properties;
using WPFCalculator.Services;

namespace WPFCalculator.ViewModels
{
    /// <summary>
    /// Provides viewModel for a calculator with basic functions
    /// </summary>
    public class SimpleCalculatorViewModel : PropertyChangedBase
    {
        #region Private fields

        private string _firstOperand;
        private string _currentOperand;
        private string _textResult;

        private bool _waitNextOperand;
        private bool _resultWasPressed;

        /// <summary>
        /// The type of specified calculator action
        /// </summary>
        private ActionType _selectedAction;

        /// <summary>
        /// Represents the functionality of the calculator
        /// </summary>
        private readonly ICalculatorEngine _calculatorEngineEngine;

        /// <summary>
        /// Encapsulates the logic of calculator
        /// </summary>
        private readonly ICalculatorFacade _calculatorFacade;

        /// <summary>
        /// The converter
        /// </summary>
        private readonly IParser _parser;

        /// <summary>
        /// A service that manages windows
        /// </summary>
        private readonly IWindowManager _windowManager;

        /// <summary>
        /// Provides the instance of the specified viewModel
        /// </summary>
        private readonly IViewModelProvider _viewModelProvider;

        #endregion

        /// <summary>
        /// Creates instance of <see cref="SimpleCalculatorViewModel"/>
        /// </summary>
        /// <param name="calculatorEngineEngine">Represents the functionality of the calculator</param>
        /// <param name="calculatorFacade">Encapsulates the logic of calculator</param>
        /// <param name="parser">The converter</param>
        /// <param name="windowManager">A service that manages windows</param>
        /// <param name="viewModelProvider">Provides the instance of the specified viewModel</param>
        public SimpleCalculatorViewModel(ICalculatorEngine calculatorEngineEngine, ICalculatorFacade calculatorFacade,
            IParser parser, IWindowManager windowManager, IViewModelProvider viewModelProvider)
        {
            _calculatorEngineEngine = calculatorEngineEngine;
            _calculatorFacade = calculatorFacade;
            _parser = parser;
            _windowManager = windowManager;
            _viewModelProvider = viewModelProvider;
            ClearResult();
            ShowStartedMessage();
        }

        #region Public methods and proporties

        /// <summary>
        /// The result block
        /// </summary>
        public string TextResult
        {
            get => _textResult;
            set
            {
                _textResult = value;
                NotifyOfPropertyChange(() => TextResult);
            }
        }

        #region Action buttons

        public void ClearButton()
        {
            ClearResult();
        }

        public void SignChangeButton()
        {
            var result = _calculatorEngineEngine.SignChange(_parser.ToDouble(TextResult));
            TextResult = _firstOperand = _parser.ToString(result);
        }

        public void PercentButton()
        {
            var result = _calculatorEngineEngine.Percent(_parser.ToDouble(TextResult));
            TextResult = _firstOperand = _parser.ToString(result);
        }

        public void DivButton()
        {
            SetActionType(ActionType.Division);
        }

        public void MultiButton()
        {
            SetActionType(ActionType.Multiplication);
        }

        public void SumButton()
        {
            SetActionType(ActionType.Sum);
        }

        public void SubButton()
        {
            SetActionType(ActionType.Subtraction);
        }

        public void ResultButton()
        {
            if (_firstOperand == CalculatorConstants.InfinityResult)
            {
                TextResult = CalculatorConstants.InfinityResult;

                return;
            }

            var result = _calculatorFacade.Process(_parser.ToDouble(_firstOperand), _parser.ToDouble(_currentOperand),
                _selectedAction);
            TextResult = _parser.ToString(result);
            _firstOperand = TextResult;
            _resultWasPressed = true;
        }

        #endregion

        #region Symbols buttons

        public void ZeroButton()
        {
            AddDigit("0");
        }

        public void OneButton()
        {
            AddDigit("1");
        }

        public void TwoButton()
        {
            AddDigit("2");
        }

        public void ThreeButton()
        {
            AddDigit("3");
        }

        public void FourButton()
        {
            AddDigit("4");
        }

        public void FifeButton()
        {
            AddDigit("5");
        }

        public void SixButton()
        {
            AddDigit("6");
        }

        public void SevenButton()
        {
            AddDigit("7");
        }

        public void EightButton()
        {
            AddDigit("8");
        }

        public void NineButton()
        {
            AddDigit("9");
        }

        public void DotButton()
        {
            AddDot();
        }

        #endregion

        /// <summary>
        /// Removes the last of the digit
        /// </summary>
        public void RemoveOneDigit()
        {
            if (TextResult == CalculatorConstants.Zero || _resultWasPressed || _waitNextOperand)
            {
                return;
            }

            TextResult = TextResult.Length > 1 
                ? TextResult.Substring(0, TextResult.Length - 1)
                : CalculatorConstants.Zero;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Adds one digits to the result block
        /// </summary>
        /// <param name="digit">The specified digit</param>
        private void AddDigit(string digit)
        {
            if (TextResult.Length > 9 && !_waitNextOperand && !_resultWasPressed)
            {
                return;
            }

            if (_resultWasPressed)
            {
                TextResult = _currentOperand = digit;
                _resultWasPressed = false;

                return;
            }

            if (TextResult != "0" && !_waitNextOperand)
            {
                _currentOperand += digit;
                TextResult += digit;

                return;
            }

            _firstOperand = TextResult;
            TextResult = _currentOperand = digit;
            _waitNextOperand = false;
        }

        /// <summary>
        /// Adds the dot to the result block
        /// <remarks>We can add only one dot</remarks>
        /// </summary>
        private void AddDot()
        {
            if (TextResult.Contains(CalculatorConstants.Dot))
            {
                return;
            }

            if (_waitNextOperand || _resultWasPressed || TextResult == CalculatorConstants.Zero ||
                TextResult == CalculatorConstants.InfinityResult)
            {
                TextResult = CalculatorConstants.ZeroDot;
                _waitNextOperand = false;
                _resultWasPressed = false;

                return;
            }

            AddDigit(CalculatorConstants.Dot);
        }

        /// <summary>
        /// Clears the result block
        /// </summary>
        private void ClearResult()
        {
            TextResult = CalculatorConstants.Zero;
            _waitNextOperand = true;
            _resultWasPressed = false;
            _currentOperand = CalculatorConstants.Zero;
            _firstOperand = CalculatorConstants.Zero;
        }

        /// <summary>
        /// Sets the selected type of the calculator action
        /// </summary>
        /// <param name="actionType">Type of the calculator action</param>
        private void SetActionType(ActionType actionType)
        {
            _waitNextOperand = true;
            _resultWasPressed = false;
            _selectedAction = actionType;
        }

        /// <summary>
        /// Shows the started message
        /// </summary>
        private async void ShowStartedMessage()
        {
            await ShowStartedMessageAsync();
        }

        /// <summary>
        /// Async shows the started message
        /// </summary>
        private async Task ShowStartedMessageAsync()
        {
            await Task.Delay(1000);

            var msgViewModel = _viewModelProvider.GetViewModel<MessageViewModel>();
            msgViewModel.MessageBlock = Resources.WelcomeMessage;

            _windowManager.ShowWindow(msgViewModel);
        }

        #endregion
    }
}
