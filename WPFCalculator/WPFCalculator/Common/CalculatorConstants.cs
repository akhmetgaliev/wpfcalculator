﻿namespace WPFCalculator.Common
{
    /// <summary>
    /// The specified constants
    /// </summary>
    public class CalculatorConstants
    {
        public const string Zero = "0";
        public const string Dot = ".";
        public const string ZeroDot = "0.";
        public const string InfinityResult = "Infinity";
    }
}