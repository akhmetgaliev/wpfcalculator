﻿namespace WPFCalculator.Common
{
    /// <summary>
    /// Types of the calculator actions
    /// </summary>
    public enum ActionType
    {
        Sum,
        Multiplication,
        Division,
        Subtraction
    }
}